package edu.westga.cs1302.exceptions.model.test;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.ShowDog;
import edu.westga.cs1302.exceptions.model.WorkingDog;

class TestShowDogConstructor {

	@Test
	void testShowDogConstructorGetName() {
		ShowDog aShowDog0 = new ShowDog("Bob", 1, 1);
		
		assertEquals(aShowDog0.getName(), "Bob");
	}
	
	@Test
	void testShowDogConstructorGetAge() {
		ShowDog aShowDog0 = new ShowDog("Bob", 1, 1);
		
		assertEquals(aShowDog0.getAge(), 1);
	}
	
	@Test
	void testShowDogConstructorGetRegistrationNumber() {
		ShowDog aShowDog0 = new ShowDog("Bob", 1, 1);

		assertEquals(aShowDog0.getRegistrationNumber(), 1);
	}
	
	@Test
	void testShowDogConstructorWithBlankName() {
		assertThrows(IllegalArgumentException.class, 
				()-> {new ShowDog("", 1, 1);}
				);
		
	}
	
	@Test
	void testShowDogConstructorWithNegativeAge() {
		assertThrows(IllegalArgumentException.class, 
				()-> {new ShowDog("Bob", -1, 1);}
				);
		
	}
	
	@Test
	void testShowDogConstructorWithNegativeRegistrationNumber() {
		assertThrows(IllegalArgumentException.class, 
				()-> {new ShowDog("Bob", 1, -1);}
				);
		
	}
	
	@Test
	void testToString() {
		ShowDog aShowDog0 = new ShowDog("Bob", 1, 1);
		assertEquals(aShowDog0.toString(), "Name: Bob Age: 1 Registration number: 1");
	}

}
