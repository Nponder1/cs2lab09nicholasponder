package edu.westga.cs1302.exceptions.model;

/** Dog superclass
 * 
 * @author Nicholas Ponder
 * @version 7/9/2021
 *
 */
public abstract class Dog {
	
	private String name;
	private int age;
	
	/** Dog constructor
	 * 
	 * @precondition aName.isEmpty() == false && anAge >= 0
	 * @param aName the dogs name
	 * @param anAge the dogs age
	 */
	public Dog(String aName, int anAge) {
		if (aName.isEmpty()) {
			throw new IllegalArgumentException("aName must not be empty");
		}
		if (anAge < 0) {
			throw new IllegalArgumentException("anAge can not be negative");
		}
		this.name = aName;
		this.age = anAge;
	}

	/** getter for this.name
	 * 
	 * @precondition none
	 * 
	 * @return this.name
	 */
	public String getName() {
		return this.name;
	}

	/** getter for this.age
	 *  
	 * @precondition none
	 *  
	 * @return this.age
	 */
	public int getAge() {
		return this.age;
	}
	
}
