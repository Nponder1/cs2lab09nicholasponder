package edu.westga.cs1302.exceptions.controllers;

import java.util.ArrayList;

import edu.westga.cs1302.exceptions.model.ShowDog;
import edu.westga.cs1302.exceptions.model.WorkingDog;

/**
 * This class defines the controller functions for the 
 * 	exceptions application
 * 
 * @author Nicholas Ponder
 * @version 7/9/2021
 *
 */
public class DogController {

	private ArrayList<String> descriptionList = new ArrayList<String>();

	/** calls the ShowDogConstructor and adds its description to the list of descriptions
	 * 
	 * @precondition anAge >= 0 && aName.isempty() == false && aRegistrationNumber >= 0
	 * @postcondition this.descriptionList.size() = @prev+1
	 * 
	 * @param name name of the dog
	 * @param age age of the dog
	 * @param registrationNumber registration number of the dog
	 */
	public void createShowDog(String name, String age, String registrationNumber) {
		String theName;
		int theAge;
		int theRegistrationNumber;
		
		if (name != null) {
			theName = name;
		} else {
			theName = "Default";
		}
		try {
			theAge = Integer.parseInt(age);
		} catch (NumberFormatException anException) {
			theAge = 0;
		}
		try {
			theRegistrationNumber = Integer.parseInt(registrationNumber);
		} catch (NumberFormatException anException) {
			theRegistrationNumber = 1;
		}
		if (theAge < 0) {
			theAge = 0;
		}
		if (theRegistrationNumber < 1) {
			theRegistrationNumber = 1;
		}
		ShowDog aShowDog = new ShowDog(theName, theAge, theRegistrationNumber);
		this.descriptionList.add(aShowDog.toString());
	}
	
	/** calls the WorkingDog constructor and adds its description to the list of descriptions
	 * 
	 * @precondition anAge >= 0 && aName.isempty() == false && maximumWorkHours >= 0
	 * @postcondition this.descriptionList.size() = @prev+1
	 * 
	 * @param name name of the dog
	 * @param age age of the dog
	 * @param maximumWorkHours maximum work hours for the dog
	 */
	public void createWorkingDog(String name, String age, String maximumWorkHours) {
		String theName;
		int theAge;
		int theMaximumWorkHours;
		
		if (name != null) {
			theName = name;
		} else {
			theName = "Default";
		}
		try {
			theAge = Integer.parseInt(age);
		} catch (NumberFormatException anException) {
			theAge = 0;
		}
		try {
			theMaximumWorkHours = Integer.parseInt(maximumWorkHours);
		} catch (NumberFormatException anException) {
			theMaximumWorkHours = 1;
		}
		if (theAge < 0) {
			theAge = 0;
		}
		if (theMaximumWorkHours < 1) {
			theMaximumWorkHours = 1;
		}
		
		WorkingDog aWorkingDog = new WorkingDog(theName, theAge, theMaximumWorkHours);
		this.descriptionList.add(aWorkingDog.toString());
	}

	/**
	 * This method will build a String representation of the 
	 * 	dogs in the collection with one dog included
	 * 	per line
	 * 
	 * @return A String holding a description of each dog, with
	 * 			one dog per line
	 */
	public String getDescription() {
		String description = "";
		for (String current : this.descriptionList) {
			description = description + current + System.lineSeparator();
		}
		return description;
	}

}
