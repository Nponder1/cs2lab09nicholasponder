package edu.westga.cs1302.exceptions.model;

/** WorkingDog constructor class
 * 
 * @author Nicholas Ponder
 * @version 7/9/2021
 *
 */
public class WorkingDog extends Dog {

	private int maximumWorkHours;
	
	/** WorkingDog Constructor
	 * 
	 * @param aName the Name of the dog
	 * @param anAge the age of the dog
	 * @param theMaximumWorkHours the dogs maximum work hours
	 */
	public WorkingDog(String aName, int anAge, int theMaximumWorkHours) {
		super(aName, anAge);
		if (theMaximumWorkHours < 1) {
			throw new IllegalArgumentException("the maximumWorkHours must be atleast 1");
		}
		this.maximumWorkHours = theMaximumWorkHours;
	}
	
	/** getter for this.maximumWorkHours
	 * 
	 * @precondition none
	 * 
	 * @return this.maximumWorkHours
	 */
	public int getMaximumWorkHours() {
		return this.maximumWorkHours;
	}
	
	/** describes the Working dog in a single line
	 * 
	 * @precondition none
	 * 
	 * @return description
	 */
	public String toString() {
		String description = "Name: " + this.getName() + " Age: " + this.getAge() + " Maximum work hours: " + this.getMaximumWorkHours();
		return description;
	}
}
