package edu.westga.cs1302.exceptions.model.test;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.WorkingDog;

class TestWorkingDogConstructor {

	@Test
	void testWorkingDogConstructorGetName() {
		WorkingDog aWorkingDog0 = new WorkingDog("Bob", 1, 1);
		
		assertEquals(aWorkingDog0.getName(), "Bob");
	}
	
	@Test
	void testWorkingDogConstructorGetAge() {
		WorkingDog aWorkingDog0 = new WorkingDog("Bob", 1, 1);
		
		assertEquals(aWorkingDog0.getAge(), 1);
	}
	
	@Test
	void testWorkingDogConstructorGetRegistrationNumber() {
		WorkingDog aWorkingDog0 = new WorkingDog("Bob", 1, 1);

		assertEquals(aWorkingDog0.getMaximumWorkHours(), 1);
	}
	
	@Test
	void testWorkingDogConstructorWithBlankName() {
		assertThrows(IllegalArgumentException.class, 
				()-> {new WorkingDog("", 1, 1);}
				);
		
	}
	
	@Test
	void testWorkingDogConstructorWithNegativeAge() {
		assertThrows(IllegalArgumentException.class, 
				()-> {new WorkingDog("Bob", -1, 1);}
				);
		
	}
	
	@Test
	void testWorkingDogConstructorWithNegativeMaximumWorkHours() {
		assertThrows(IllegalArgumentException.class, 
				()-> {new WorkingDog("Bob", 1, -1);}
				);
		
	}
	
	@Test
	void testToString() {
		WorkingDog aWorkingDog0 = new WorkingDog("Bob", 1, 20);
		assertEquals(aWorkingDog0.toString(), "Name: Bob Age: 1 Maximum work hours: 20");
	}

}
