package edu.westga.cs1302.exceptions.model;

/** ShowDog constructor class
 * 
 * @author Nicholas Ponder
 * @version 7/9/2021
 *
 */
public class ShowDog extends Dog {
	
	private int registrationNumber;

	/** ShowDog constructor
	 * 
	 * @precondition aName.isEmpty() == false && anAge >= 0 && aRegistrationNumber > 0
	 * 
	 * @param aName the dogs name
	 * @param anAge the dogs age
	 * @param aRegistrationNumber the registration number
	 */
	public ShowDog(String aName, int anAge, int aRegistrationNumber) {
		super(aName, anAge);	
		if (aRegistrationNumber < 1) {
			throw new IllegalArgumentException("aRegistrationNumber must be atleast 1");
		}
		this.registrationNumber = aRegistrationNumber;
	}

	/** getter for this.registrationNumber
	 * 
	 * @precondition none
	 * 
	 * @return this.registrationNumber
	 */
	public int getRegistrationNumber() {
		return this.registrationNumber;
	}
	
	/** describes the show dog in a single line
	 * 
	 * @precondition none
	 * 
	 * @return description
	 */
	public String toString() {
		String description = "Name: " + this.getName() + " Age: " + this.getAge() + " Registration number: " + this.getRegistrationNumber();
		return description;
	}

}
